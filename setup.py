#!/usr/bin/env python3

from setuptools import setup

import toolbox

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = [
    "progressbar2 >= 3.34.3"
]

setup(
    name="Circuit Toolbox",
    version=toolbox.__version__,
    description=toolbox.DESCRIPTION,
    long_description=readme,
    author="Sean Leavey",
    author_email="electronics@attackllama.com",
    url="https://git.ligo.org/sean-leavey/circuit-toolbox",
    packages=[
        "toolbox"
    ],
    entry_points={
        'console_scripts': [
            'circuit-toolbox = toolbox.__main__:main'
        ]
    },
    install_requires=requirements,
    license="GPLv3",
    zip_safe=False,
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5"
    ]
)
