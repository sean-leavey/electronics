import locale

# use default locale (required for number formatting in log warnings)
locale.setlocale(locale.LC_ALL, "")

__version__ = "0.2.0"
DESCRIPTION = "Resistor/regulator calculator utility"
PROGRAM = "circuit-toolbox"
