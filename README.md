# Electronics Toolbox
Calculator to find the best sets of series/parallel resistors to match a desired
resistance or regulator output voltage.

## Installation
```bash
pip3 install git+https://git.ligo.org/sean-leavey/circuit-toolbox.git
```

## Usage
Start with the tool's help function to list available commands:
```bash
> circuit-toolbox --help
```

### Find close matching voltage division ratio
You want to precisely divide a certain voltage. You can do this with:
```bash
> circuit-toolbox rratio <ratio>
```
The program will calculate closest matching resistor divider ratios. Say, for
example, that you want to generate 3.5V from a 10V rail. This is a ratio of
0.35, so you would enter:
```bash
> circuit-toolbox rratio 0.35
```
This suggests:
```
Closest 3 matches for 0.35 target (R1, R2; ratio = R2 / (R1 + R2)):
1. ratio = (0.3488372093023256, 5.60 Ω ± 5.00%, 3.00 Ω ± 5.00%) (-0.33%)
2. ratio = (0.3488372093023256, 56.0 Ω ± 5.00%, 30.0 Ω ± 5.00%) (-0.33%)
3. ratio = (0.3488372093023256, 560 Ω ± 5.00%, 300 Ω ± 5.00%) (-0.33%)
```
By default, E24 series resistors are assumed with exponents between 0 (e.g. 1 Ω)
and 6 (e.g. 10 MΩ). While the above resistors would give the desired ratio, it's
unlikely in most applications that such low resistors would be desired. Set the
minimum and maximum exponents with `--min-exp` and `--max-exp`, respectively:
```bash
> circuit-toolbox rratio 0.35 --min-exp 4 --max-exp 4
```
```
Closest 3 matches for 0.35 target (R1, R2; ratio = R2 / (R1 + R2)):
1. ratio = (0.3488372093023256, 56.0 kΩ ± 5.00%, 30.0 kΩ ± 5.00%) (-0.33%)
2. ratio = (0.35135135135135137, 24.0 kΩ ± 5.00%, 13.0 kΩ ± 5.00%) (+0.39%)
3. ratio = (0.34782608695652173, 30.0 kΩ ± 5.00%, 16.0 kΩ ± 5.00%) (-0.62%)
```
Now the resistors are large enough that significant current is not dissipated
in most signal applications, but the suggested resistors still don't match the
ratio closely. Specify a series with more available values with the `-s` flag:
```bash
> circuit-toolbox rratio 0.35 --min-exp 4 --max-exp 4 -s E48
```
```
Closest 3 matches for 0.35 target (R1, R2; ratio = R2 / (R1 + R2)):
1. ratio = (0.3498920086393089, 30.1 kΩ ± 5.00%, 16.2 kΩ ± 5.00%) (-0.03%)
2. ratio = (0.34953271028037386, 34.8 kΩ ± 5.00%, 18.7 kΩ ± 5.00%) (-0.13%)
3. ratio = (0.3493761140819964, 36.5 kΩ ± 5.00%, 19.6 kΩ ± 5.00%) (-0.18%)
```
We've reduced the error by an order of magnitude. For this particular ratio,
E192 is the lowest set that offers an exact ratio (with e.g. 23.4 kΩ and
12.6 kΩ).

You can ask for more or fewer matches with `-n`, and specify series and parallel
combinations with `--min-series`, `--max-series`, `--min-parallel` and
`--max-parallel`. For most applications this won't be necessary, but if you only
have a small resistor set available this can help to find closer combinations.
For example, we can achieve an exact ratio match with E24 resistors by using
two resistors in parallel:
```bash
> circuit-toolbox rratio 0.35 --min-exp 4 --max-exp 4 --max-parallel 2
```
```
Closest 3 matches for 0.35 target (R1, R2; ratio = R2 / (R1 + R2)):
1. ratio = (0.35000000000000003, 14.9 kΩ ± 3.63% (24.0 kΩ ± 5.00% || 39.0 kΩ ± 5.00%), 8.00 kΩ ± 3.73% (12.0 kΩ ± 5.00% || 24.0 kΩ ± 5.00%)) (+0.00%)
2. ratio = (0.35000000000000003, 14.9 kΩ ± 3.63% (24.0 kΩ ± 5.00% || 39.0 kΩ ± 5.00%), 8.00 kΩ ± 3.54% (16.0 kΩ ± 5.00% || 16.0 kΩ ± 5.00%)) (+0.00%)
3. ratio = (0.35000000000000003, 15.4 kΩ ± 3.57% (27.0 kΩ ± 5.00% || 36.0 kΩ ± 5.00%), 8.31 kΩ ± 3.79% (12.0 kΩ ± 5.00% || 27.0 kΩ ± 5.00%)) (+0.00%)
```

Note that when these flags are used, the number of combinations and permutations
of resistors increases *greatly*, and therefore also computation time. A warning
will display on screen if the computation will take a long time. If this is the
case, you may wish to cancel execution and adjust the flag settings.

## Credits
Sean Leavey  
<electronics@attackllama.com>
